# Descarga CFDI del SAT

## El uso de este proyecto tiene un costo de $100.00 anuales

## Script para descargar de forma automática documentos electrónicos del SAT.

### Mira el [wiki](https://gitlab.com/mauriciobaeza/cfdi-descarga/wikis/home) para los detalles de uso.

### Ayudanos a ayudar a otros, conoce [nuestras actividades](http://universolibre.org/hacemos/).


## IMPORTANTE

**El SAT a publicado información de un nuevo webservice para descargar los
documentos, aún no han fecha para su puesta en marcha, pero en cuanto lo
liberen lo implementaremos a la brevedad**

[Nuevo webservice del SAT](https://www.sat.gob.mx/consultas/42968/consulta-y-recuperacion-de-comprobantes-(nuevo))


### Su ayuda es importante para mantener este proyecto.


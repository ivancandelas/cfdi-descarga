# Lista de cambios

Para una lista completa visita el [repositorio del proyecto](https://gitlab.com/mauriciobaeza/cfdi-descarga/activity).


## v 4.4.0
---
* Todo esta de nuevo operativo. Ahora el único método para identificarse es la FIEL

## v 4.3.0
---
* El SAT ha retirado el captcha, pero hay algunos cambios, es necesario actualizar a esta versión.

## v 4.2.0
---
* El SAT cambio algunos parametros de acceso. Ya no permite la descarga masiva, pero si la consulta.

## v 4.1.0
---
* Se agregan algunas validaciones para la FIEL

## v 2.0.0
---
* Se agrega interfaz gráfica

## v 1.3.0
---
* Se agrega pdftotext para windows

## v 1.2.1
---
* Se actualiza la URL del SAT de descarga

## v 1.2.0
---
* Se agrega soporte para descargar solo nomina

## v 1.1.1
---
* FIX - En el rango de búsqueda en recibidas

## v 1.1.0
---
* Se mejora el algoritmo para buscar por fecha, en vez de segmentar en días, se
segmenta por mes, en emisores con pocos documentos, esto mejora mucho la velocidad
de búsqueda
* Se mejora el formato del log

## v 1.0.0
---
Se reescribieron varias partes del código, sobre todo las relacionadas con la
conexión al SAT, la cual no se caracteriza por su fiabilidad.

1. En algunas ocasiones, el primer intento de identificación falla, aun y cuando
las credenciales sean correctas. En este caso se reintenta un máximo de tres
veces la conexión. Este valor es configurable en el archivo **settings.py**

2. Cuando se intenta descargar el XML, o bien el SAT no responde o se alcanza el
tiempo de espera, en este caso también se intenta un máximo de tres veces de
forma predeterminada, o el valor establecido en **settings.py**

Con estas dos opciones, se ha mejorado mucho la fiabilidad de las descargas.

* Ahora se descarga toda la información mostrada por el SAT al realizar una búsqueda.
* Ahora se descargan los acuses de cancelación en PDF.
* El SAT no despliega la fecha de cancelación de CFDI emitidos. La misma se toma del PDF de acuse de cancelación. Se requiere [pdftotext](http://www.foolabs.com/xpdf/download.html) para esto.
* Ahora se guardan en la base de datos, tanto la lista de CFDIs descargados, como la lista de búsquedas realizadas, así como el total de documentos encontrados en cada búsqueda, esto permite hacer validaciones para garantizar que el total de cada búsqueda corresponde con el total de documentos descargados.
* Ahora se puede usar [fades](https://fades.readthedocs.io/en/release-5/) para ejecutar el script, **fades** permite ejecutar de forma muy fácil el script dentro de un entorno virtual sin complicaciones.


## v 0.5.0
---
Esta versión esta originalmente en [Github](https://github.com/UniversoLibreMexicoAC/descargar-cfdi)
pero no tiene más mantenimiento mi mejoras.
